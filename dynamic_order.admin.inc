﻿<?php
/**
 * Settings of module
*/

function dynamic_form_admin_settings() {
  $types = node_type_get_types();
  $option_id = 0;
  foreach($types as $node_type) {
    $options [$option_id] = $node_type->type;
	$option_id ++;
  }
  variable_set('dynamic_order_node_types_list', $options);
  $form['dynamic_order_node_types'] = array(
    '#type' => 'select',
	'#title' => t('Choose content type: '),
	'#default_value' => variable_get('dynamic_order_node_types', 0),
	'#options' =>   $options,
    '#description' => t('Content type to display as products for the customer'),
  );
  $form['dynamic_order_custom_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom path of the form'),
    '#default_value' => variable_get('dynamic_order_custom_path', 'dynamic_order'),
    '#maxlength' => 30,
  );
  $form['target_email'] = array (
    '#type' => 'textfield',
	'#title' => t('Admin e-mail'),
	'#default_value' => get_default_value_email(),
	'#size' =>   60,
	'#maxlength' => 128,
    '#description' => t('Email on which order should be send'),
  );
  
  $form['#submit'][] = 'dynamic_order_admin_settings_submit';
  return system_settings_form($form);
}
function dynamic_order_admin_settings_submit($form, $form_state) {
  
}
/**
* Show saved 'mailto' email adres
*/
function get_default_value_email() {
  $site_mail = variable_get('site_mail', '');
  $order_email = variable_get('target_email', '');
  if(empty($order_email)) {
    $email = $site_mail;
  }else {
    $email = $order_email;
  }
  return $email;
}